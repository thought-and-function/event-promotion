## Intro

The Event Promotion DAPP is intended for Event Creators (Event Managers) to use as a platform to allow Event Promoters to help with promotion of events. Event Promoters are 'followed' by regular Users, and any events that a Promoter is promoting will be visible to their followers so that they can buy tickets.

Event Creators can set a ticket price and comission fee. Event Promoters will receive the comission fee if tickets are bought through them.

The DAPP has been developed to a point where a user can use the UI to:

**Event Management**
- Register as an Event Manager using an ETH address
- Create events and set ticket price, comission fee, number of tickets, and event end date.
- View revenue of an event through ticket sales on the platform.

**Event Promoter**
- Register as an Event Promoter using an ETH address
- View all events that have been created by all Event Managers
- Register to 'promote' for an event

**User (Follower)**
- Register as a user using an ETH address
- View all promoters
- View all events that the promoters being followed are promoting
- Buy a ticket for an event

Functionality that doesn't exist on the UI but does exist in the smart contracts:

 - Withdrawing ETH from event contract into an Event Mananger contract
   address 
 - Withdrawing ETH from an Event Mananger contract into an Event
   Manager's user address 
 - Withdrawing ETH comission from an event contract into an Event Promoter contract address 
 - Withdrawing ETH comission from an Event Promoter address into an Event Promoter's user address

Functionality that doesn't exist yet in both the UI and smart contracts:
- Restricting withdrawal until an event has finished
- Cancelling an event and refunding ticket sales

## Running Instructions

**Running the backend (smart contracts):**

Run ganache-cli on default port 8545 and migrate contracts
```
ganache-cli
truffle migrate
```

**Running the UI:**

Running locally:
`npm run start`

Running from a build:
```
npm run build
npm install -g pushstate-server
pushstate-server build_webpack
open http://localhost:9000
```
  

## The Demo

Create an event as an Event Manager:
1. Click the 'Creator' link, enter an ETH address into the input box and click Register.
2. Confirm the transaction.
3. Click 'select' to select that Event Manager user that you just created. It's possible to add multiple Event Managers (for demo purposes).
4. Enter an event end date, update other fields if needed, and click Create.
5. Confirm the transaction. You should see your event listed on the right. Click 'Refresh events' if not.

Register to promote an event as an Event Promoter
1. Click the 'Promoter' link, enter an ETH address into the input box and click Register.
2. Confirm the transaction.
3. Click 'select' to select that Event Promoter user that you just created. It's possible to add multiple Event Promoters (for demo purposes).
4. You should see a list of all events created by all Event Creators.
5. Click 'Register to promote' on an event that you'd like to promote.
6. Confirm the transaction.
7. Click 'Refresh my events' after 2-3 seconds to see the event listed in the right most column.

Follow promoters and buy tickets
1. Click the 'Follower' link, enter an ETH address into the input and click Register.
2. Confirm the transaction.
3. The list of 'Other Promoters' should be populated. Click 'Follow' to follow a promoter. 
4. The list of events in the right most column will be updated with events. Click 'Refresh Events' if not.
5. Click 'Buy Ticket' to buy a ticket.

View your updated revenue as an Event Creator
1. Click the 'Creator' link to view the Event Manager dashboard.
2. Click 'Refresh Events' in the right most column to see the Revenue value update for event you purchased a ticket for.


## Design patterns
**Circuit Breaker pattern**
The Orchestrator contract inherits the Zeppelin 'Pausable' contract which allows the owner to disable and enable the registering of new users through a toggle function.

**Owner pattern / Access restriction patterns**
All smart contracts implement the owner pattern to ensure certain functions access are limited to only the owner of the contract.

Event smart contract also ensures that only Event Manangers (the contract that created the event), and Event Promoters can call certain functions. 

Event Manager contracts and Event Promoter contracts are owned by the Orchestrator contract, but access restriction patterns are used on various functions which restrict access to the actual manager and promoter users. (see constructor of Event Mananger and Event Promoter where user address is passed in as an argument). 

**PullPayment withdrawal pattern**
The Event contract uses the PullPayment withdrawal pattern inherited from the PullPayment contract from the Zeppelin library. This ensures that attackers can't trap the contract by implementing functions that can cause the contract to get into a bad state.

## Protecting against common attacks
**Reentrancy**
Using the PullPayment contract from Zeppelin library protects against reentrancy attacks when Event Mananger and Event Promoter user's withdraw money from an event.

**Cross function race conditions**
As a Promoter's balance is held inside an Event until they withdraw it, two functions share the same state - Event.buyTicket and Event.withdrawPayments (inherited). This can lead to Cross function race conditions if not careful. Inherting from the Zeppelin PullPayment contract also helps to prevents this as the Escrow contract which PullPayment inherits sets the users balance to 0 before attempting the transfer.

**Forcibly Receiving Ether**
The Event contract uses a fallback function in cases where ETH is received incorrectly.

## Libraries
**Open Zeppelin**
This project uses the Owner and PullPayment contracts from the OpenZeppelin npm library: https://github.com/OpenZeppelin/openzeppelin-solidity. I've also installed the Zeppelin library from EthPM but it was quite old so I decided to use the OpenZeppelin one.



## Stretch Goals
**IPFS**
The static files of the UI are held on IPFS: https://ipfs.io/ipfs/Qmb6oA8hFN5Vk5QGvt1qPw598EmjzhUYGRVPnUzokFs1Em

**Rinkeby TestNet**
The UI thats hosted on IPFS is pointing to the Rinkeby network. You'll need to use a Rinkeby account to interact with the blockchain.






