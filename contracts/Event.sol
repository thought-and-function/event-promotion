pragma solidity ^0.4.24;

import "openzeppelin-solidity/contracts/ownership/Ownable.sol";
import "openzeppelin-solidity/contracts/payment/PullPayment.sol";

contract Event is Ownable, PullPayment {
    uint storedData;
    uint totalNumberOfTickets;
    uint availableTickets;
    uint commissionFee;
    uint ticketPrice;
    uint endDate;

    mapping (address => address) private tickets;
    mapping (address => bool) private registeredPromoters;
    mapping (address => uint) private promoterBalances;

    event LogTicketBought(address sender, address promoter);
    event LogWithdrawal(address promoter, uint amount);
    
    modifier verifyPromoter (address _address) {
        require(registeredPromoters[_address] == true, "must be a registered promotr");
        _;
    }
    
    modifier verifyNotPromoter (address _address) {
        require(registeredPromoters[_address] == false, "must be a registered promoter");
        _;
    }

    modifier verifyOwner (address _address) {
        require(owner == _address, "must be owner");
        _;
    }

    constructor(uint _totalNumberOfTickets, uint _comissionFee, uint _ticketPrice, uint _endDate) public {
        require(_comissionFee <= _ticketPrice, "comission fee too large");
        totalNumberOfTickets = _totalNumberOfTickets;
        availableTickets = _totalNumberOfTickets;
        commissionFee = _comissionFee;
        ticketPrice = _ticketPrice;
        endDate = _endDate;
    }

    function withdrawBalance() public payable verifyOwner(msg.sender) {
        msg.sender.transfer(address(this).balance);
    }

    function registerAsPromoter() public {
        registeredPromoters[msg.sender] = true;
    }

    function getEventBalance() public view verifyOwner(msg.sender) returns (uint) {
        return address(this).balance;
    }
    
    function buyTicket(address _promoterAddress) public payable verifyPromoter(_promoterAddress) {
        require (msg.value >= ticketPrice && msg.value >= commissionFee, "value must be greater or equal to ticket price");
        asyncTransfer(_promoterAddress, commissionFee);
        tickets[msg.sender] = _promoterAddress;
        emit LogTicketBought(msg.sender, _promoterAddress);
    }

    function getTicket() public view returns (bool) {
        return tickets[msg.sender] != address(0);
    }

    function getEvent() public view returns (uint, uint, uint, uint, uint, address) {
        return (totalNumberOfTickets, availableTickets, commissionFee, ticketPrice, endDate, owner);
    }

    function() public payable {
        revert("no match");
    }
}