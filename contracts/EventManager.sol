pragma solidity ^0.4.24;

import "openzeppelin-solidity/contracts/ownership/Ownable.sol";
import "../interfaces/Orchestrator.sol";

import { Event } from "./Event.sol";

contract EventManager is Ownable {
    address manager;

    address[] public events;

    event LogEventCreated(address eventAddress);

    modifier verifyManager(address _address) {
        require(_address == manager, "must be manager");
        _;
    }

    constructor(address _manager) public {
        manager = _manager;
    }

    function create(uint _totalNumberOfTickets, uint _comissionFee, uint _ticketPrice, uint _endDate) public {
        address eventAddress = new Event(_totalNumberOfTickets, _comissionFee, _ticketPrice, _endDate);
        events.push(eventAddress);
        OrchestratorInterface(owner).registerEvent(eventAddress);
        emit LogEventCreated(eventAddress);
    }

    function withdrawBalanceFromEvent (address _eventAddress) public payable verifyManager(msg.sender) {
        Event ev = Event(_eventAddress);
        ev.withdrawBalance();
    }

    function withdraw() public verifyManager(msg.sender) {
        msg.sender.transfer(address(this).balance);
    }

    function getEvents() public view returns (address[]) {
        return events;
    }
}
