pragma solidity ^0.4.24;

import "openzeppelin-solidity/contracts/lifecycle/Pausable.sol";

import { EventManager } from "./EventManager.sol";
import { Promoter } from "./Promoter.sol";
import { User } from "./User.sol";

contract Orchestrator is Pausable {
    mapping(address => address) registeredManagers;
    mapping(address => address) registeredPromoters;
    mapping(address => address) registeredUsers;
    address[] public managers;
    address[] public promoters;
    address[] public events;
    address[] public users;

    event LogManagerRegistered(address managerAddress);
    event LogPromoterRegistered(address promoterAddress);
    event LogUserRegistered(address userAddress);
    
    modifier verifyAddress(address _address) {
        require(_address != address(0), "must be a valid address");
        _;
    }

    function registerEventManager() public whenNotPaused() returns (address) {
        if (registeredManagers[msg.sender] != address(0)) {
            return registeredManagers[msg.sender];
        }

        address ev = new EventManager(msg.sender);
        registeredManagers[msg.sender] = ev;
        managers.push(ev);
        emit LogManagerRegistered(ev);
        return ev;
    }

    function getManagerAddress() public view returns (address) {
        return registeredManagers[msg.sender];
    }

    function registerPromoter() public whenNotPaused() returns (address) {
        if (registeredPromoters[msg.sender] != address(0)) {
            return registeredPromoters[msg.sender];
        }

        address promoter = new Promoter(msg.sender);
        registeredPromoters[msg.sender] = promoter;
        promoters.push(promoter);
        emit LogPromoterRegistered(promoter);
        return promoter;
    }

    function getPromoterAddress() public view returns (address) {
        return registeredPromoters[msg.sender];
    }

    function registerUser() public whenNotPaused() returns (address) {
        if (registeredUsers[msg.sender] != address(0)) {
            return registeredUsers[msg.sender];
        }

        address user = new User(msg.sender);
        registeredUsers[msg.sender] = user;
        users.push(user);
        emit LogUserRegistered(user);
        return user;
    }

    function getUserAddress() public view returns (address) {
        return registeredUsers[msg.sender];
    }

    function registerEvent(address _eventAddress) public verifyAddress(_eventAddress) {
        events.push(_eventAddress);
    }

    function getManagers() public view returns (address[]) {
        return managers;
    }

    function getPromoters() public view returns (address[]) {
        return promoters;
    }

    function getEvents() public view returns (address[]) {
        return events;
    }
}
