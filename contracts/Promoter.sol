pragma solidity ^0.4.24;

import "openzeppelin-solidity/contracts/ownership/Ownable.sol";

import { Event } from "./Event.sol";

contract Promoter is Ownable {
    address promoter;
    mapping(address => bool) registeredEvents;
    address[] public events;
    address[] public followers;

    constructor(address _promoter) public {
        promoter = _promoter;
    }

    event LogRegisteredAsPromoter(address eventAddress);

    modifier verifyPromoter(address _address) {
        require(_address == promoter, "must be promoter");
        _;
    }

    modifier verifyNotRegistered(address _address) {
        require(registeredEvents[_address], "already registered");
        _;
    }

    function registerToPromoteEvent(address _eventAddress) public verifyPromoter(msg.sender) {
        Event ev = Event(_eventAddress);
        ev.registerAsPromoter();
        events.push(_eventAddress);
        emit LogRegisteredAsPromoter(_eventAddress);
    }

    function withdrawComissionFromEvent(address _eventAddress) public payable {
        Event ev = Event(_eventAddress);
        ev.withdrawPayments();
    }

    function withdrawBalance() public payable verifyPromoter(msg.sender) {
        msg.sender.transfer(address(this).balance);
    }
    
    function getEvents() public view returns (address[]) {
        return events;
    }

    function followPromoter() public {
        followers.push(msg.sender);
    }

    function getFollowers() public view verifyPromoter(msg.sender) returns (address[]) {
        return followers;
    }
}
