pragma solidity ^0.4.24;

import "openzeppelin-solidity/contracts/ownership/Ownable.sol";

import { Promoter } from "./Promoter.sol";

contract User is Ownable {
    address user;

    mapping (address => bool) followed;
    address[] public promoters;

    constructor(address _user) public {
        user = _user;
    }

    event LogFollowing(address promoterAddress);

    modifier verifyUser(address _address) {
        require(_address == user, "must be user");
        _;
    }

    modifier verifyNotFollowed(address _address) {
        require(followed[_address] == false, "already following");
        _;
    }

    function followPromoter(address _promoterAddress) public {
        Promoter promoter = Promoter(_promoterAddress);
        promoter.followPromoter();
        followed[_promoterAddress] = true;
        promoters.push(_promoterAddress);
        emit LogFollowing(_promoterAddress);
    }

    function getPromoters() public view returns (address[]) {
        return promoters;
    }
}
