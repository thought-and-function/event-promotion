var SimpleStorage = artifacts.require("./SimpleStorage.sol");
var Orchestrator = artifacts.require("./Orchestrator.sol");

module.exports = function(deployer) {
  deployer.deploy(SimpleStorage);
  deployer.deploy(Orchestrator);
};
