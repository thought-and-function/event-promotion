import React, { Component } from 'react'
import { connect } from 'react-redux';
import { Link } from 'react-router'
import { HiddenOnlyAuth, VisibleOnlyAuth } from './util/wrappers.js'

// UI Components
import LoginButtonContainer from './user/ui/loginbutton/LoginButtonContainer'
import LogoutButtonContainer from './user/ui/logoutbutton/LogoutButtonContainer'

// Styles
import './css/oswald.css'
import './css/open-sans.css'
import './css/pure-min.css'
import './App.css'

class App extends Component {
  componentDidMount() {
  }
  
  render() {
    const { demo } = this.props;

    const OnlyAuthLinks = VisibleOnlyAuth(() =>
      <span>
        <li className="pure-menu-item">
          <Link to="/creator" className="pure-menu-link">Creator</Link>
        </li>
        <li className="pure-menu-item">
          <Link to="/promoter" className="pure-menu-link">Promoter</Link>
        </li>
        <li className="pure-menu-item">
          <Link to="/follower" className="pure-menu-link">Follower</Link>
        </li>
      </span>
    )

    const OnlyGuestLinks = HiddenOnlyAuth(() =>
      <span>
        <LoginButtonContainer />
      </span>
    )

    return (
      <div className="App">
        <nav className="navbar pure-menu pure-menu-horizontal" style={{ zIndex: 99 }}>
          <Link to="/" className="pure-menu-heading pure-menu-link">Event Promotion - Home</Link>
          <ul className="pure-menu-list navbar-right">
            <OnlyGuestLinks />
            <OnlyAuthLinks />
          </ul>
        </nav>
        {this.props.children}
      </div>
    );
  }
}

export default connect((state) => ({
}),{ })(App);
