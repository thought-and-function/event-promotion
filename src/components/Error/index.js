import React, { Component } from 'react';
import styled from 'styled-components';

const ErrorWrapper = styled.div`

`;

const ErrorMessage = styled.p`

`;

class Error extends Component {
  render() {
    const { message } = this.props.error;
    return (
      <ErrorWrapper>
        <ErrorMessage>{message}</ErrorMessage>
      </ErrorWrapper>
    );
  }
}

export default Error;