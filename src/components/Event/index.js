import styled from 'styled-components';
import variables from 'css/variables';

export const Card = styled.div`
    border: 1px solid ${variables.color1};
    border-radius: 6px;
    padding: 16px;
    width: 100%;
    box-sizing: border-box;
`;

export const CardTitle = styled.h4`
    margin-top: 0;
`;

export const CardDescription = styled.p`

`;

export const CardPrice = styled.p`

`;

export const CardActions = styled.div`

`;

export const CardAction = styled.button`

`;

export const CardDate = styled.span`

`;