import React from 'react';

import styled from 'styled-components';

const Label = styled.label`
  margin-bottom: 5px;
  display: inline-block;
`;

export const FieldWrapper = styled.div`
  margin-bottom: 16px;
`;

export const Field = ({label, children, name, ...props}) => (
  <FieldWrapper>
    {label && <Label htmlFor={name}>{label}</Label>}
    {children}
  </FieldWrapper>
);

export const TextInput = ({ label, ...props}) => (
  <Field label={label}>
    <TextInputEl {...props} />
  </Field>
);

export const DateInput = ({ label, ...props}) => (
  <Field label={label}>
    <DateInputEl {...props} />
  </Field>
);

export const NumberInput = ({ label, ...props}) => (
  <Field label={label}>
    <NumberInputEl {...props} />
  </Field>
);


export const InputEl = styled.input`
  width: 100%;
`;

export const TextInputEl = InputEl.extend.attrs({
  type: 'text',
})`
  
`;

export const DateInputEl = InputEl.extend.attrs({
  type: 'date',
})`
  
`;

export const NumberInputEl = InputEl.extend.attrs({
  type: 'number',
})`
  
`;

export const TextArea = ({ label, ...props}) => (
  <Field label={label}>
    <TextAreaEl {...props} />
  </Field>
);

export const TextAreaEl = styled.textarea`
  width: 100%;
`;