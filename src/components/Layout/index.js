import styled from 'styled-components';

export const TwoColumn = styled.div`
  display: flex;
  ${props => props.wrap && `flex-wrap: ${props.wrap === true ? `wrap` : props.wrap};`}
`;

export const Column = styled.div`
  ${props => props.grow && `
    flex: 1 1 auto;
    width: 100%;
  `}
`;