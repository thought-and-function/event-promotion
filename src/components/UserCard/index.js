import React, { Component } from 'react';
import PropTypes from 'prop-types'
import styled from 'styled-components';
import variables from 'css/variables';

import DefaultError from '../Error';

const Error = styled(DefaultError)`
  width: 100%;
  height: 100%;
  background: black;
  opacity: 0.5;
  color: white;
  position: absolute;
  top: 0;
  left: 0;
  z-index: 5;
`;

const Card = styled.div`
  position: relative;
  max-width: 260px;
  border: 1px solid ${variables.color1};
  border-radius: 6px;
  padding: 6px;
  margin-bottom: 8px;

  ${props => props.isSelected  && `border-color: green;`}
`;

const Row = styled.div`
  display: flex;
  flex-direction: column;
  margin-bottom: 5px;
`;

const Label = styled.label`
  font-weight: bold;
  margin-right: 6px;
`;

const Value = styled.div`
  overflow: hidden;
  text-overflow: ellipsis;
`;

const InnerValue = Value.extend`
  margin-left: 8px;
`;

export const UserCardActions = styled.div`

`;

export const UserCardAction = styled.button`
  
`;

class UserCard extends Component {
  static defaultProps = {
    data: PropTypes.object.isRequired,
  }

  renderObject(obj) {
    return Object.keys(obj).map((key) => {
      if (!obj[key]) {
        return null;
      }

      if (obj[key].constructor === Array) {
        return (
          <Row key={key}>
            <Label><strong>{key}:</strong></Label>
            {
              obj[key].map(item => <InnerValue>{obj[key]}</InnerValue>)
            }
          </Row>
        );
      }

      if (obj[key] && typeof obj[key] === 'object') {
        return (
          <Row key={key}>
            <Label><strong>{key}:</strong></Label>
            <InnerValue>{this.renderObject(obj[key])}</InnerValue>
          </Row>
        );
      }

      return (
        <Row key={key}>
          <Label>{key}:</Label>
          <Value>{obj[key] && obj[key].toString()}</Value>
        </Row>
      );
    });
  }

  render() {
    const { isSelected, data, children } = this.props;

    if (!data) {
      return null;
    }

    const { error, ...rest } = data;

    return (
      <Card isSelected={isSelected}>
        {
          error && <Error error={error} />
        }
        {this.renderObject(rest)}
        {children}
      </Card>
    );
  }
}

export default UserCard;