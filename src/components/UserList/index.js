import React, { Component } from 'react';

import UserCard, { UserCardActions, UserCardAction } from 'components/UserCard';

class UserList extends Component {
  handleSelectClicked = (address) => {
    this.props.selectUser(address);
  }

  handleCancelClicked = (address) => {
    this.props.cancelRegistration(address);
  }

  render() {
    const { users, selectedUser, selectUser, selectLabel, cancelRegistration } = this.props;

    if (!users) {
      return null;
    }

    return (
      <div>
        {
          Object.keys(users).map((address) => {
            return (
              <UserCard 
                key={address}
                data={users[address]}
                isSelected={selectedUser && selectedUser.address === address}
              >
                <UserCardActions>
                  { selectUser && <UserCardAction onClick={() => this.handleSelectClicked(address)}>{ selectLabel || 'Select' }</UserCardAction> }
                  { cancelRegistration && <UserCardAction onClick={() => this.handleCancelClicked(address)}>Cancel</UserCardAction> }
                </UserCardActions>
              </UserCard>
            );
          })
        }
      </div>
    );
  }
}

export default UserList;