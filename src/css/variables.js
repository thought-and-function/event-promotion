export default {
    color1: 'rgba(216, 219, 226, 1)',
    color2: 'rgba(169, 188, 208, 1)',
    color3: 'rgba(88, 164, 176, 1)',
    color4: 'rgba(55, 63, 81, 1)',
    color5: 'rgba(27, 27, 30, 1)',
};