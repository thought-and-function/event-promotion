import React from 'react';
import ReactDOM from 'react-dom';
import { Router, Route, IndexRoute, hashHistory } from 'react-router'
import { Provider } from 'react-redux'
import { syncHistoryWithStore } from 'react-router-redux'
import { UserIsAuthenticated } from './util/wrappers.js'

// Layouts
import App from './App'
import Home from './layouts/home/Home'
import PromoterDashboard from './layouts/promoter/Dashboard'
import CreatorDashboard from './layouts/creator/Dashboard'
import FollowerDashboard from './layouts/follower/Dashboard'

// Redux Store
import store from './redux/store'

const history = syncHistoryWithStore(hashHistory, store)

ReactDOM.render((
    <Provider store={store}>
      <Router history={history}>
        <Route path="/" component={App}>
          <IndexRoute component={Home} />
          <Route path="creator" component={UserIsAuthenticated(CreatorDashboard)} />
          <Route path="promoter" component={UserIsAuthenticated(PromoterDashboard)} />
          <Route path="follower" component={UserIsAuthenticated(FollowerDashboard)} />
        </Route>
      </Router>
    </Provider>
  ),
  document.getElementById('root')
)
