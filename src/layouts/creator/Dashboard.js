import React, { Component } from 'react';
import { connect } from 'react-redux';
import { createSelector } from 'reselect';

import { 
  createEvent,
  cancelRegistration,
  cancelEvent,
  selectManager,
  refreshEvents,
} from 'redux/actions/managerActions';
import { 
  registerManager
} from 'redux/actions/orchestratorActions';
import EventCreation from './ui/EventCreation';
import EventList from './ui/EventList';
import EventCard from './ui/EventCard';
import Managers from './ui/Managers';
import RegisterManager from './ui/RegisterManager';

import * as styled from './ui/components';

class Dashboard extends Component {
  handleRefreshMyEvents = () => {
    this.props.refreshEvents(this.props.selectedManager);
  }

  render() {
    const { 
      events,
      createEvent,
      cancelEvent,
      registerManager,
      managers,
      selectManager,
      selectedManager,
      cancelRegistration,
      balances,
    } = this.props;

    const myEvents = selectedManager && events.data && Object.values(events.data)
      .filter(x => x.managerAddress === selectedManager.managerAddress);

    return (
      <main className="container">
        <div className="pure-g">
          <div className="pure-u-1-1">
            <h1>Creator Dashboard</h1>
            <p>This dashboard shows future events that have been created by currently logged in creator.</p>
            <h2>Register a Manager account</h2>
            <RegisterManager register={registerManager} />
            {
              managers && Object.keys(managers).length > 0 &&
                <styled.TwoColumn>
                  <styled.Column style={{ marginRight: 16 }}>
                    <h2>Managers</h2>
                    {
                      <Managers 
                        managers={managers} 
                        selectedManager={selectedManager} 
                        selectManager={selectManager}
                        cancelRegistration={cancelRegistration}
                      />
                    }
                  </styled.Column>
                {
                  selectedManager &&
                    <styled.Column grow>
                      <styled.TwoColumn>
                        <styled.Column grow style={{ maxWidth: 400, marginRight: 8 }}>
                          <h2>Create New Event</h2>
                          <EventCreation manager={selectedManager} createEvent={createEvent} />
                        </styled.Column>
                        <styled.Column grow>
                          <h2>My Events <button onClick={this.handleRefreshMyEvents}>Refresh Events</button></h2>
                          <EventList>
                            {
                              myEvents && myEvents.map((event) => (
                                <EventCard 
                                  key={event.eventAddress}
                                  {...event}
                                  cancelEvent={cancelEvent}
                                  balance={balances && balances[event.eventAddress]}
                                />
                              ))
                            }
                          </EventList>
                        </styled.Column>
                      </styled.TwoColumn>
                    </styled.Column>
                }
              </styled.TwoColumn>
            }
          </div>
        </div>
      </main>
    );
  }
}

const selectedManagerSelector = createSelector(
  (state) => state.users.selectedManagerAddress,
  (state) => state.users.managers,
  (selectedManagerAddress, managers) => {
    return selectedManagerAddress && managers[selectedManagerAddress];
  }
);

const selectBalances = createSelector(
  selectedManagerSelector,
  (selectedManager) => {
    return selectedManager && selectedManager.balances;
  }
);

export default connect((state) => ({
  managers: state.users.managers,
  selectedManager: selectedManagerSelector(state),
  events: state.events,
  balances: selectBalances(state),
}), { 
  createEvent,
  cancelEvent,
  registerManager,
  cancelRegistration,
  selectManager,
  refreshEvents,
})(Dashboard);