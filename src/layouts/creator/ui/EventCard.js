import React, { Component } from 'react';
import PropTypes from 'prop-types';

import * as event from 'components/Event';

class EventCard extends Component {
  static propTypes = {
    address: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired,
    date: PropTypes.string.isRequired,
    description: PropTypes.string.isRequired,
    price: PropTypes.string.isRequired,
    cancelEvent: PropTypes.func.isRequired,
  }

  handleCancelEvent = () => {
    const { address } = this.props;
    this.props.cancelEvent(address);
  }

  render() {
    const { 
      ticketCount,
      endDate,
      comissionFee,
      ticketPrice,
      balance,
    } = this.props;

    return (
      <event.Card>
        <event.CardTitle>TicketCount: {ticketCount}</event.CardTitle>
        <event.CardDate>End Date: {new Date(endDate).toDateString()}</event.CardDate>
        <event.CardDescription>ComissionFee: {comissionFee}</event.CardDescription>
        <event.CardPrice>Price: ETH {ticketPrice}</event.CardPrice>
        <event.CardPrice>Revenue: ETH {balance}</event.CardPrice>
      </event.Card>
    );
  }
}

export default EventCard;