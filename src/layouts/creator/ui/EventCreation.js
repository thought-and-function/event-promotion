import React, { Component } from 'react';
import PropTypes from 'prop-types';

import * as event from 'components/Event';
import * as form from 'components/Form';
import * as components from './components';

class EventCreation extends Component {
  static propTypes = {
    manager: PropTypes.object.isRequired,
    createEvent: PropTypes.func.isRequired,
  }

  constructor(props) {
    super(props);

    this.state = {
      ticketCount: 1000,
      comission: 0.1,
      date: undefined,
      price: 1,
    };
  }

  createChangeHandler = (field) => {
    return (e) => {
      this.setState({
        [field]: e.target.value,
      });
    };
  }

  canCreate = () => {
    const { ticketCount, date, comission, price } = this.state;
    return ticketCount && date && comission && price;
  }

  handleCreateClicked = () => {
    const { address, managerAddress } = this.props.manager;
    this.props.createEvent(address, managerAddress, this.state);
  }

  render() {
    const { ticketCount, comission, date, price } = this.state;

    return (
      <event.Card>
         <components.TwoColumn>
          <components.Column style={{ marginRight: 16, width: `50%` }}>
          <form.NumberInput onChange={this.createChangeHandler('ticketCount')} label={'Tickets No.'} value={ticketCount}></form.NumberInput>    
            <form.NumberInput onChange={this.createChangeHandler('price')} label={'Price (ETH)'} value={price}></form.NumberInput>    
          </components.Column>
          <components.Column grow>
            <form.DateInput onChange={this.createChangeHandler('date')} label={'End Date'} value={date}></form.DateInput>  
            <form.NumberInput onChange={this.createChangeHandler('comission')} label={'comission (ETH)'} value={comission}></form.NumberInput>
          </components.Column>
        </components.TwoColumn>
        <event.CardActions>
          <event.CardAction onClick={this.handleCreateClicked} disabled={!this.canCreate()}>Create</event.CardAction>
        </event.CardActions>
      </event.Card>
    );
  }
}

export default EventCreation;