import React, { Component } from 'react';
import PropTypes from 'prop-types';

import UserCard, { UserCardActions, UserCardAction } from 'components/UserCard';

class Managers extends Component {
  static defaultProps = {
    managers: PropTypes.array.isRequired,
    selectedManager: PropTypes.string,
    selectManager: PropTypes.func.isRequired,
    cancelRegistration: PropTypes.func.isRequired,
  }

  handleSelectClicked = (address) => {
    this.props.selectManager(address);
  }

  handleCancelClicked = (address) => {
    this.props.cancelRegistration(address);
  }

  render() {
    const { managers, selectedManager } = this.props;

    if (!managers) {
      return null;
    }

    return (
      <div>
        {
          Object.keys(managers).map((address) => {
            return (
              <UserCard 
                key={address}
                data={managers[address]}
                isSelected={selectedManager && selectedManager.address === address}
              >
                <UserCardActions>
                  <UserCardAction onClick={() => this.handleSelectClicked(address)}>Select</UserCardAction>
                  <UserCardAction onClick={() => this.handleCancelClicked(address)}>Cancel</UserCardAction>
                </UserCardActions>
              </UserCard>
            );
          })
        }
      </div>
    );
  }
}

export default Managers;