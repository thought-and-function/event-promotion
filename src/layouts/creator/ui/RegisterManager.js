import React, { Component } from 'react';
import PropTypes from 'prop-types'

import * as form from 'components/Form';

class RegisterManager extends Component {
  static defaultProps = {
    register: PropTypes.func.isRequired,
  }
  
  constructor(props) {
    super(props);
    this.state = {
      address: '',
    };
  }

  handleAddressChanged = (e) => {
    this.setState({
      address: e.target.value,
    });
  };

  handleRegisterClicked = () => {
    this.props.register(this.state.address);
  }

  render() {
    const { address } = this.state;
    return (
      <div>
        <form.TextInput onChange={this.handleAddressChanged} label={'Address'} value={address}></form.TextInput>
        <button onClick={this.handleRegisterClicked}>Register</button>
      </div>
    );
  }
}

export default RegisterManager;