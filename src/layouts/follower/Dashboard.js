import React, { Component } from 'react';
import { connect } from 'react-redux';

import { registerUser } from 'redux/actions/orchestratorActions';
import { purchaseTicket } from 'redux/actions/eventActions';
import { followPromoter, getPromoters, refreshEvents } from 'redux/actions/userActions';

import * as Layout from 'components/Layout';
import RegisterUserType from 'components/RegisterUserType';
import UserList from 'components/UserList';
import UserCard from 'components/UserCard';
import EventCard from './ui/EventCard';
import EventList from './ui/EventList';

class Dashboard extends Component {
  handleFollowPromoter = (promoterAddress) => {
    this.props.followPromoter(promoterAddress);
  }

  handleUnfollowPromoter = (promoterAddress) => {
    
  }

  handleRefreshEvents = () => {
    this.props.refreshEvents();
  }

  handleRefreshMyPromoters = () => {
    this.props.getPromoters();
  }

  render() {
    const { 
      user,
      promoters,
      following,
      registerUser,
      events,
      purchaseTicket,
     } = this.props;
    
    return (
      <main className="container">
        <div className="pure-g">
          <div className="pure-u-1-1">
            <h1>Follower Dashboard</h1>
            <p>This dashboard shows future events that have been created by promoters that this logged in 'follower' is following.</p>
            <h2>Register your user</h2>
            <RegisterUserType register={registerUser} />
            <Layout.TwoColumn>
              <Layout.Column style={{ minWidth: 360 }}>
                <h2>User</h2>
                { <UserCard data={{ address: user.address, userAddress: user.userAddress }} /> }
              </Layout.Column>
              <Layout.Column>
                <Layout.TwoColumn>
                  <Layout.Column style={{ minWidth: 360 }}>
                    <h2>Promoters Followed <button onClick={this.handleRefreshMyPromoters}>Refresh</button></h2>
                    {
                      <UserList 
                        users={following}
                        selectUser={this.handleUnfollowPromoter}
                        selectLabel="Unfollow"
                      />
                    }
                    <h2>Other Promoters</h2>
                    {
                      <UserList 
                        users={promoters}
                        selectUser={this.handleFollowPromoter}
                        selectLabel="Follow"
                      />
                    }
                  </Layout.Column>
                  <Layout.Column style={{ minWidth: 460 }}>
                    <h2>Events <button onClick={this.handleRefreshEvents}>Refresh Events</button></h2>
                    <EventList>
                      {
                        events && Object.keys(events).map(eventAddress => (
                          <EventCard key={eventAddress} {...events[eventAddress]} purchaseTicket={purchaseTicket} />
                        ))
                      }
                    </EventList>
                  </Layout.Column>
                </Layout.TwoColumn>
              </Layout.Column>
            </Layout.TwoColumn>
          </div>
        </div>
      </main>
    );
  }
}


export default connect((state) => ({
  events: state.user.events,
  user: state.user,
  promoters: state.user.promoters,
  following: state.user.following,
}), { purchaseTicket, registerUser, followPromoter, getPromoters, refreshEvents })(Dashboard);