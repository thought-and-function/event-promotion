import React, { Component } from 'react';
import PropTypes from 'prop-types';

import * as event from 'components/Event';

class EventCard extends Component {
  static propTypes = {
    address: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired,
    date: PropTypes.string.isRequired,
    description: PropTypes.string.isRequired,
    price: PropTypes.string.isRequired,
    purchaseTicket: PropTypes.func.isRequired,
  }

  handlePurchaseClicked = () => {
    this.props.purchaseTicket(this.props);
  }

  render() {
    const { 
      ticketCount,
      endDate,
      comissionFee,
      ticketPrice,
    } = this.props.event;
    const { purchased } = this.props

    return (
      <event.Card>
        <event.CardTitle>TicketCount: {ticketCount}</event.CardTitle>
        <event.CardDate>End Date: {new Date(endDate).toDateString()}</event.CardDate>
        <event.CardDescription>ComissionFee: {comissionFee}</event.CardDescription>
        <event.CardPrice>Price: ETH {ticketPrice}</event.CardPrice>
        <event.CardActions>
          {
            purchased
              ? `1 ticket purchased`
              : <event.CardAction onClick={this.handlePurchaseClicked}>Purchase Ticket</event.CardAction>
          }
        </event.CardActions>
      </event.Card>
    );
  }
}

export default EventCard;