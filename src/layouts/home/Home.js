import React, { Component } from 'react'
import { Link } from 'react-router';

class Home extends Component {
  render() {
    return(
      <main className="container" style={{ maxWidth: 600 }}>
        <h2>
          <Link to="/creator">Creator</Link>
        </h2>
        <p>Navigate to this page to register your ETH address as an event manager and create an event.</p>
        <h2>
          <Link to="/promoter">Promoter</Link>
        </h2>
        <p>Navigate to this page after creating events. Register an ETH address as an event promoter and you'll see availalbe events to promote.</p>
        <h2>
          <Link to="/follower">Follower</Link>
        </h2>
        <p>Navigate to this page once you have promoted an event. Register an ETH address as a user and select a promoter to follow. You'll be able to see the events they're promoting and purchase a ticket.</p>
      </main>
    )
  }
}

export default Home
