import React, { Component } from 'react'
import { connect } from 'react-redux';
import { createSelector } from 'reselect';

import { 
  registerPromoter
} from 'redux/actions/orchestratorActions';
import { 
  selectPromoter,
  cancelRegistration,
  registerToPromoteEvent,
  getPromoterEvents,
} from 'redux/actions/promoterActions';

import RegisterUserType from 'components/RegisterUserType';

import UserList from 'components/UserList';
import * as Layout from 'components/Layout';
import EventCard from './ui/EventCard';
import EventList from './ui/EventList';

class Dashboard extends Component {
  constructor(props, { authData }) {
    super(props)
    authData = this.props
  }

  handleRefreshMyEvents = () => {
    const { getPromoterEvents } = this.props;
    const { address, promoterAddress } = this.props.selectedPromoter;
    getPromoterEvents(address, promoterAddress);
  }

  render() {
    const { 
      registerToPromoteEvent,
      registerPromoter,
      promoters,
      selectedPromoter,
      selectPromoter,
      events,
    } = this.props;

    return(
      <main className="container">
        <div className="pure-g">
          <div className="pure-u-1-1">
            <h1>Promoter Dashboard</h1>
            <p>This dashboard shows future events that are available to a 'promoter' user to promote to their followers.</p>
            <h2>Register Promoter Address</h2>
            <RegisterUserType register={registerPromoter} />
            {
              promoters && Object.keys(promoters).length > 0 &&
                <Layout.TwoColumn>
                  <Layout.Column style={{ marginRight: 16 }}>
                    <h2>Promoters</h2>
                    {
                      <UserList 
                        users={promoters} 
                        selectedUser={selectedPromoter} 
                        selectUser={selectPromoter}
                        cancelRegistration={cancelRegistration}
                      />
                    }
                  </Layout.Column>
                  {
                    selectedPromoter &&
                      <Layout.Column>
                        <Layout.TwoColumn>
                          <Layout.Column style={{ marginRight: 8, minWidth: 460 }}>
                            <h2>Upcoming Events</h2>
                            <EventList>
                              {
                                events.data && Object.values(events.data).reduce((prev, item) => {
                                  if (selectedPromoter.events && selectedPromoter.events.find(x => x === item.eventAddress)) {
                                    return prev;
                                  } 

                                  prev.push(
                                    <EventCard 
                                      key={item.eventAddress} 
                                      {...item} 
                                      selectedPromoter={selectedPromoter}
                                      registerAsPromoter={registerToPromoteEvent}
                                    />
                                  );

                                  return prev;
                                }, [])
                              }
                            </EventList>
                          </Layout.Column>
                          <Layout.Column>
                            <h2>Promoted Events <button onClick={this.handleRefreshMyEvents}>Refresh my events</button></h2>
                            <EventList>
                              {
                                selectedPromoter && selectedPromoter.events
                                  && selectedPromoter.events.map((address) => (
                                    <EventCard 
                                      key={address} 
                                      {...events.data[address]}
                                      promoting
                                    />
                                  ))
                              }
                            </EventList>
                          </Layout.Column>
                        </Layout.TwoColumn>
                      </Layout.Column>
                  }
                </Layout.TwoColumn>
            }
          </div>
        </div>
      </main>
    )
  }
}

const selectedPromoterSelector = createSelector(
  (state) => state.users.selectedPromoterAddress,
  (state) => state.users.promoters,
  (selectedPromoterAddress, promoters) => {
    return selectedPromoterAddress && promoters[selectedPromoterAddress];
  },
);

export default connect((state) => ({
  events: state.events,
  promoters: state.users.promoters,
  selectedPromoter: selectedPromoterSelector(state),
}), { 
  registerToPromoteEvent,
  registerPromoter,
  selectPromoter,
  cancelRegistration,
  getPromoterEvents,
})(Dashboard);
