import React, { Component } from 'react';
import PropTypes from 'prop-types';

import * as event from 'components/Event';

class EventCard extends Component {
  static propTypes = {
    address: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired,
    date: PropTypes.number.isRequired,
    description: PropTypes.string.isRequired,
    price: PropTypes.string.isRequired,
    registerAsPromoter: PropTypes.func.isRequired,
    unregisterAsPromoter: PropTypes.func.isRequired,
    promoting: PropTypes.bool,
  };

  handleRegisterClicked = () => {
    const { address, promoterAddress } = this.props.selectedPromoter;
    const { eventAddress } = this.props;

    this.props.registerAsPromoter(address, promoterAddress, eventAddress);
  }

  handleUnregisterClicked = () => {
    const { address } = this.props;
    this.props.unregisterAsPromoter(address);
  }

  render() {
    const { 
      ticketCount,
      endDate,
      comissionFee,
      ticketPrice,
      promoting,
      managerAddress
    } = this.props;

    return (
      <event.Card>
        <event.CardTitle>TicketCount: {ticketCount}</event.CardTitle>
        <event.CardDate>End Date: {new Date(endDate).toDateString()}</event.CardDate>
        <event.CardDescription>ComissionFee: {comissionFee}</event.CardDescription>
        <event.CardPrice>Price: ETH {ticketPrice}</event.CardPrice>
        <event.CardDescription>Manager: {managerAddress}</event.CardDescription>
        <event.CardActions>
          {
            promoting
              ? null
              : <event.CardAction onClick={this.handleRegisterClicked}>Register as promoter</event.CardAction>
          }
        </event.CardActions>
      </event.Card>
    );
  }
}

export default EventCard;