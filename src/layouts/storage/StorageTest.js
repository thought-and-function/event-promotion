import React, { Component } from 'react';
import { connect } from 'react-redux';

import { set } from 'redux/actions/storageActions';

class StorageTest extends Component {
  constructor(props) {
    super(props);
    this.state = {
      value: 0,
    };
  }

  handleInputChanged = (e) => {
    this.setState({
      value: e.target.value,
    });
  }

  handleSetClicked = () => {
    this.props.set(this.state.value);
  }

  render() {
    const { 
      value,
      pendingValue,
      error,
      isSetting,
    } = this.props.storage;

    return (
      <div>
        <div>Value: {value}</div>    
        <div>Pending Value: {pendingValue}</div>
        <div>Is Setting: {isSetting}</div>
        <input onChange={this.handleInputChanged} value={this.state.value}></input>
        <button onClick={this.handleSetClicked}>Set</button>
        <br/>
        <div>Error:</div>
        <div>{error}</div>
      </div>
    );
  }
}

export default connect((state) => ({
  storage: state.storage,
}), { set })(StorageTest);