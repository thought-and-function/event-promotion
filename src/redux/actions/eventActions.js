import GetEventContract from '../../util/EventContract';
import { weiToEth, ethToWei } from 'util/conversions';

export const GET_EVENTS = "EventActions/get_events";
export const GET_EVENTS_FAILED = "EventActions/get_event_failed";

export const GET_EVENT = "EventActions/get_event";
export const GET_EVENT_SUCCESS = "EventActions/get_event_success";
export const GET_EVENT_FAILED = "EventActions/get_event_failed";

export const PURCHASE_TICKET = "EventActions/purchase_ticket";
export const PURCHASE_TICKET_SUCCESS = "EventActions/purchase_ticket_success";

export async function getEventPurchasedStatus(userAddress, eventAddress) {
  const evContract = await GetEventContract(eventAddress);
  return await evContract.getTicket({ from: userAddress });
}

export function purchaseTicket(eventData) {
  return async (dispatch, getState) => {
    const { address } = getState().user;
    const evContract = await GetEventContract(eventData.eventAddress);
    const tx = await evContract.buyTicket(eventData.promoterAddress, { from: address, value: ethToWei(eventData.event.ticketPrice) });
    if (tx.logs[0].args.sender === address && tx.logs[0].args.promoter === eventData.promoterAddress) {
      dispatch({
        type: PURCHASE_TICKET_SUCCESS,
        eventAddress: eventData.eventAddress,
      });
    }
  }
}

export async function getEventBalance(eventAddress, managerAddress) {
  const evContract = await GetEventContract(eventAddress);
  const balance = await evContract.getEventBalance({ from: managerAddress });
  return balance.toNumber();
}

export async function getEventData(eventAddress) {
  const evContract = await GetEventContract(eventAddress);
  const eventInfo = await evContract.getEvent();
  return {
    eventAddress,
    event: {
      eventAddress,
      ticketCount: eventInfo[0].toNumber(),
      availableTickets: eventInfo[1].toNumber(),
      comissionFee: weiToEth(eventInfo[2].toNumber()),
      ticketPrice: weiToEth(eventInfo[3].toNumber()),
      endDate: new Date(eventInfo[4].toNumber()),
      managerAddress: eventInfo[5],
    },
  };
}

export function getEvent(eventAddress) {
  return async (dispatch) => {
    dispatch({
      type: GET_EVENT,
      eventAddress,
    });

    try {
      const eventData = await getEventData(eventAddress);
      dispatch({
        type: GET_EVENT_SUCCESS,
        ...eventData,
      });
    } catch (error) {
      dispatch({
        type: GET_EVENT_FAILED,
        eventAddress,
        error,
      });
    }
  };
}

export function getEvents(eventAddresses) {
  return async (dispatch) => {
    dispatch({
      type: GET_EVENTS,
      eventAddresses,
    });

    try {
      for(let i = 0; i < eventAddresses.length ; i++) {
        dispatch(getEvent(eventAddresses[i]));
      }
    } catch (error) {
      dispatch( {
        type: GET_EVENTS_FAILED,
        error,
      });
    }
  }
}

export function registerAsPromoter(eventAddress) {

}