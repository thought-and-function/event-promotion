import OrchestratorContract from 'util/OrchestratorContract';
import GetEventManagerContract from 'util/EventManagerContract';
import { ethToWei, weiToEth } from 'util/conversions';
import { getEvents, getEventBalance } from './eventActions';

export const REGISTER_MANAGER = 'ManagerActions/register_manager';
export const REGISTER_MANAGER_SUCCESS = 'ManagerActions/register_manager_success';
export const REGISTER_MANAGER_FAILED = 'ManagerActions/register_manager_failed';
export const CANCEL_MANAGER_REGISTRATION = 'ManagerActions/cancel_manager_registration';
export const SELECT_MANAGER = 'ManagerActions/select_manager';
export const CREATE_EVENT = 'ManagerActions/create_event';
export const CREATE_EVENT_SUCCESS = 'ManagerActions/create_event_success';
export const CREATE_EVENT_FAILED = 'ManagerActions/create_event_failed';
export const CANCEL_EVENT = 'ManagerActions/cancel_event';
export const GET_EVENT_BALANCE_SUCCESS = 'ManagerActions/get_event_balance_success';

export function refreshEvents(manager) {
  return async (dispatch) => {
    const eventManager = await GetEventManagerContract(manager.managerAddress);
    const eventAddresses = await eventManager.getEvents();
    dispatch(getEvents(eventAddresses));
    for(let i = 0; i < eventAddresses.length; i++) {
      const balance = await getEventBalance(eventAddresses[i], manager.managerAddress);
      dispatch({
        type: GET_EVENT_BALANCE_SUCCESS,
        ...manager,
        eventAddress: eventAddresses[i],
        balance: weiToEth(balance),
      });
    }
  }
}

export function cancelRegistration(address) {
  return {
    type: CANCEL_MANAGER_REGISTRATION,
    address,
  };
}

export function selectManager(address) {
  return {
    type: SELECT_MANAGER,
    address,
  };
}

export function getManagerEvents(managerAddress) {
  return async (dispatch) => {
  const eventManager = await GetEventManagerContract(managerAddress);
  const eventAddresses = await eventManager.getEvents();
  dispatch(getEvents(eventAddresses));
  }
}

 export function createEvent(address, managerAddress, eventData) {
   return async (dispatch) => {
     dispatch({
       address,
       type: CREATE_EVENT,
     });

     try {
      const eventManager = await GetEventManagerContract(managerAddress);
      
      debugger;

      await eventManager.create(
        eventData.ticketCount,
        ethToWei(eventData.comission),
        ethToWei(eventData.price),
        (new Date(eventData.date)).getTime(),
        { from: address },
      );

      dispatch(refreshEvents({ address, managerAddress}));

      dispatch({
        type: CREATE_EVENT_SUCCESS,
        address,
      });
     } catch (error) {
       dispatch({
         type: CREATE_EVENT_FAILED,
         address,
         error,
       });
     }
   }
 }