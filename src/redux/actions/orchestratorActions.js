import OrchestratorContract from 'util/OrchestratorContract';
import { getManagerEvents } from './managerActions';
import { getPromoters, getAllPromoters } from './userActions';

export const REGISTER_MANAGER = 'ManagerActions/register_manager';
export const REGISTER_MANAGER_SUCCESS = 'ManagerActions/register_manager_success';
export const REGISTER_MANAGER_FAILED = 'ManagerActions/register_manager_failed';

export const REGISTER_PROMOTER = 'PromoterActions/register_promoter';
export const REGISTER_PROMOTER_SUCCESS = 'PromoterActions/register_promoter_success';
export const REGISTER_PROMOTER_FAILED = 'PromoterActions/register_promoter_failed';

export const REGISTER_USER = 'UserActions/register_user';
export const REGISTER_USER_SUCCESS = 'UserActions/register_user_success';
export const REGISTER_USER_FAILED = 'UserActions/register_user_failed';

export function registerUser(address) {
  return async (dispatch) => {
    const orchestrator = await OrchestratorContract;
    const types = [REGISTER_USER, REGISTER_USER_SUCCESS, REGISTER_USER_FAILED];
    const reigsterFn = orchestrator.registerUser;
    const getDataFn = orchestrator.getUserAddress;
    const onSuccess = (contractAddress) => {
      dispatch(getPromoters(contractAddress));
      dispatch(getAllPromoters());
    };

    return dispatch(register(address, types, reigsterFn, getDataFn, onSuccess));
  }
}

export function registerManager(address) {
  return async (dispatch) => {
    const orchestrator = await OrchestratorContract;
    const types = [REGISTER_MANAGER, REGISTER_MANAGER_SUCCESS, REGISTER_MANAGER_FAILED];
    const registerFn = orchestrator.registerEventManager;
    const getDataFn = orchestrator.getManagerAddress;
    const onSuccess = (contractAddress) => {
      dispatch(getManagerEvents(contractAddress));
    };

    return dispatch(register(address, types, registerFn, getDataFn, onSuccess));
  }
}

export function registerPromoter(address) {
  return async (dispatch) => {
    const orchestrator = await OrchestratorContract;
    const types = [REGISTER_PROMOTER, REGISTER_PROMOTER_SUCCESS, REGISTER_PROMOTER_FAILED];
    const registerFn = orchestrator.registerPromoter;
    const getDataFn = orchestrator.getPromoterAddress;

    return dispatch(register(address, types, registerFn, getDataFn));
  }
}

export function register(address, types, registerFn, getDataFn, onSuccess) {
  const [REQUEST, SUCCESS, FAILED] = types;

  return async (dispatch) => {
    dispatch({
      type: REQUEST,
      address,
    });

    try {
      const contractAddress = await registerFn({ from: address })
        .then(() => {
          return getDataFn({ from: address })
            .then((val) => {
              return val;
            })
        }); 

      if (onSuccess) {
        onSuccess(contractAddress);
      }

      dispatch({
        type: SUCCESS,
        address,
        contractAddress,
      });
    } catch(error) {
      dispatch({
        type: FAILED,
        address,
        error,
      });
    };
  };
 }