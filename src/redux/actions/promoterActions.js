import GetPromoterContract from 'util/PromoterContract';
import { getEvents } from './eventActions';

export const SELECT_PROMOTER = 'PromoterActions/select_promoter';
export const CANCEL_PROMOTER_REGISTRATION = 'PromoterActions/cancel_promoter_registration';
export const REGISTER_TO_PROMOTE_EVENT = 'PromoterActions/register_to_promote_event';
export const REGISTER_TO_PROMOTE_EVENT_SUCCESS = 'PromoterActions/register_to_promote_event_success';
export const REGISTER_TO_PROMOTE_EVENT_FAILED = 'PromoterActions/register_to_promote_event_failed';
export const GET_PROMOTER_EVENTS = 'PromoterActions/get_promoter_events';
export const GET_PROMOTER_EVENTS_SUCCESS = 'PromoterActions/get_promoter_events_success';
export const GET_PROMOTER_EVENTS_FAILED = 'PromoterActions/get_promoter_events_failed';

export function cancelRegistration(address) {
  return {
    type: CANCEL_PROMOTER_REGISTRATION,
    address,
  };
}

export function selectPromoter(address) {
  return {
    type: SELECT_PROMOTER,
    address,
  };
}

export async function getPromoterEventAddresses(promoterAddress) {
  const promoterContract = await GetPromoterContract(promoterAddress);
  return await promoterContract.getEvents();
}

export function getPromoterEvents(address, promoterAddress) {
  return async (dispatch) => {
    dispatch({
      type: GET_PROMOTER_EVENTS,
      address,
    });

    try {
      const events = await getPromoterEventAddresses(promoterAddress);
      dispatch(getEvents(events));

      dispatch({
        type: GET_PROMOTER_EVENTS_SUCCESS,
        address,
        events,
      });
    } catch (error) {
      dispatch({
        type: GET_PROMOTER_EVENTS_FAILED,
        address,
        error,
      });
    }
  }
}
export function registerToPromoteEvent(address, promoterAddress, eventAddress) {
  return async (dispatch) => {
    dispatch({
      type: REGISTER_TO_PROMOTE_EVENT,
      address,
      eventAddress,
    });

    try {
      const promoterContract = await GetPromoterContract(promoterAddress);
      await promoterContract.registerToPromoteEvent(eventAddress, { from: address });
      const events = await promoterContract.getEvents();
      
      dispatch({
        type: REGISTER_TO_PROMOTE_EVENT_SUCCESS,
        address,
        eventAddress,
        events,
      });
    } catch (error) {
      dispatch({
        type: REGISTER_TO_PROMOTE_EVENT_FAILED,
        address,
        eventAddress,
        error,
      });
    }
  }
}