import SimpleStorageContract, { getAccounts } from '../../util/SimpleStorageContract';

export const SET_VALUE = 'StorageActions/SET_VALUE';
export const SET_VALUE_SUCCESS = 'StorageActions/SET_VALUE_SUCCESS';
export const SET_VALUE_FAILED = 'StorageActions/SET_VALUE_FAILED';

export const set = (value) => {
  return async (dispatch) => {
    dispatch({
      type: SET_VALUE,
      value,
    });

    const ssc = await SimpleStorageContract;
    debugger;
    const accounts = getAccounts();

    ssc.set(value, { from: accounts[0] })
      .then(() => {
        ssc.get()
          .then((val) => {
            dispatch({
              type: SET_VALUE_SUCCESS,
              value: val.toNumber(),
            });
          })
          .catch((error) => {
            dispatch({
              type: SET_VALUE_FAILED,
              error,
            });
            throw error;
          });
      })
      .catch((error) => {
        dispatch({
          type: SET_VALUE_FAILED,
          error,
        });
        throw error;
      });
  }
}