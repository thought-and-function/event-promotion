import OrchestratorContract from 'util/OrchestratorContract';
import GetUserContract from 'util/UserContract';
import { getPromoterEventAddresses } from './promoterActions';
import { getEventData, getEventPurchasedStatus } from './eventActions';

export const GET_PROMOTERS = 'UserActions/get_promoters';
export const GET_PROMOTERS_SUCCESS = 'UserActions/get_promoters_success';
export const GET_PROMOTERS_FAILED = 'UserActions/get_promoters_failed';
export const FOLLOW_PROMOTER = 'UserActions/follow_promoter';
export const FOLLOW_PROMOTER_SUCCESS = 'UserActions/follow_promoter_success';
export const FOLLOW_PROMOTER_FAILED = 'UserActions/follow_promoter_failed';
export const GET_PROMOTER_EVENTS = 'UserActions/get_promoter_events';
export const GET_PROMOTER_EVENTS_SUCCESS = 'UserActions/get_promoter_events_success';
export const GET_PROMOTER_EVENTS_FAILED = 'UserActions/get_promoter_events_failed';
export const GET_ALL_PROMOTERS_SUCCESS = 'UserActions/get_all_promoters_success';
export const GET_PURCHASE_STATUS_SUCCESS = 'UserActions/get_purchase_status_success';

export const getAllPromoters = () => {
  return async (dispatch) => {
    const orchestrator = await OrchestratorContract;
    const promoters = await orchestrator.getPromoters();
    dispatch({
      type: GET_ALL_PROMOTERS_SUCCESS,
      promoters,
    });
  }
}

export const getPromoters = (userAddress) => {
  return async (dispatch, getState) => {
    if (!userAddress) {
      userAddress = getState().user.userAddress;
    }

    dispatch({
      type: GET_PROMOTERS,
    });

    try {
      const userContract = await GetUserContract(userAddress);
      const promoters = await userContract.getPromoters();

      dispatch({
        type: GET_PROMOTERS_SUCCESS,
        promoters,
      });
    } catch (error) {
      dispatch({
        type: GET_PROMOTERS_FAILED,
        error,
      });
    }
  }
}

export const updatePurchasedStatus = () => {
  return async (dispatch, getState) => {
    const user = getState().user;
    const events = Object.keys(user.events);
    for(let i = 0; i < events.length; i++) {
      const isPurchased = await getEventPurchasedStatus(user.address, events[i]);
      dispatch({
        type: GET_PURCHASE_STATUS_SUCCESS,
        eventAddress: events[i],
        isPurchased,
      });
    }
  }
}

export const getPromoterEvents = (promoterAddress) => {
  return async (dispatch) => {
    const events = await getPromoterEventAddresses(promoterAddress);
    for(let i = 0; i < events.length; i++) {
      dispatch({
        type: GET_PROMOTER_EVENTS,
        promoterAddress,
      });
      try {
        const eventData = await getEventData(events[i]);
        dispatch({
          type: GET_PROMOTER_EVENTS_SUCCESS,
          promoterAddress,
          event: eventData,
        });
      } catch(error) {
        dispatch({
          type: GET_PROMOTER_EVENTS_FAILED,
          promoterAddress,
          error,
        });
      }
    }

    dispatch(updatePurchasedStatus());
  }
}

export const followPromoter = (promoterAddress) => {
  return async (dispatch, getState) => {
    const user = getState().user;

    dispatch({
      type: FOLLOW_PROMOTER,
      promoterAddress,
    });

    try {
      const userContract = await GetUserContract(user.userAddress);
      const tx = await userContract.followPromoter(promoterAddress, {from: user.address});
      debugger;
      if (tx.logs[0].args.promoterAddress == promoterAddress) {
        dispatch({
          type: FOLLOW_PROMOTER_SUCCESS,
          promoterAddress,
        });

        dispatch(getPromoterEvents(promoterAddress));
      }
    } catch (error) {
      dispatch({
        type: FOLLOW_PROMOTER_FAILED,
        promoterAddress,
      })
    }
  }
}

export const refreshEvents = () => {
  return async (dispatch, getState) => {
    const following = Object.keys(getState().user.following);
    for(let i = 0; i < following.length; i++) {
      dispatch(getPromoterEvents(following[i]));
    }
  }
}