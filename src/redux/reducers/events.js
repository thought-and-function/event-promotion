import {
  PROMOTE_EVENT,
  UN_PROMOTE_EVENT,
  TICKET_PURCHASED,
  EVENT_CANCELLED,
  EVENT_CREATED,
  GET_EVENT_SUCCESS,
} from '../actions/eventActions';

const today = new Date();
const currentDate = today.getDate();

// const initialState = {
//   data: [{
//     title: `Lorem Ipsum`,
//     date: new Date(today.setDate(currentDate + 10)).toISOString(),
//     description: `
//       Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque et quam bibendum, ultrices velit non, dictum turpis. Cras at vehicula felis. 
//       Mauris pharetra lobortis nulla, a pretium dui imperdiet id. 
//     `,
//     price: `10`,
//     address: `some_address`,
//     promoting: false,
//     purchased: false,
//   }],
// };

const initialState = {
  data: null,
};

const eventsReducer = (state = initialState, action) => {
  switch(action.type) {
    case GET_EVENT_SUCCESS: {
      return Object.assign({}, state, {
        data: {
          ...state.data,
          [action.eventAddress]: {
            ...action.event,
          },
        },
      });
    }
    
    default:
      return state;
  }
}

export default eventsReducer
