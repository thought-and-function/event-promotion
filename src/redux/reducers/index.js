import { combineReducers } from 'redux'
import { routerReducer } from 'react-router-redux'
import user from './user';
import users from './users';
import events from './events';
import storage from './storage';

const appReducer = combineReducers({
  routing: routerReducer,
  user,
  users,
  events,
  storage,
})

const rootReducer = (state, action) => {
  if (action.type === 'USER_LOGGED_OUT') {
    window.localStorage.removeItem(`event-promotion`);
    state = undefined;
  }

  return appReducer(state, action);
}

export default rootReducer;
