import {
  SET_VALUE,
  SET_VALUE_FAILED,
  SET_VALUE_SUCCESS,
} from '../actions/storageActions';

const initialState = {
  isSetting: false,
  value: 0,
  pendingValue: null,
  error: null,
};

const storageReducer = (state = initialState, action) => {
  switch(action.type) {
    case SET_VALUE: {
      return Object.assign({}, state, { isSetting: true, pendingValue: action.value, error: null });
    }
    
    case SET_VALUE_SUCCESS: {
      return Object.assign({}, state, { isSetting: false, value: action.value, pendingValue: null });
    }

    case SET_VALUE_FAILED: {
      return Object.assign({}, state, { isSetting: false, error: action.error });
    }
    
    default:
      return state;
  }
}

export default storageReducer
