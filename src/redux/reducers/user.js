
import {
  REGISTER_USER,
  REGISTER_USER_SUCCESS,
  REGISTER_USER_FAILED,
} from '../actions/orchestratorActions';

import {
  GET_PROMOTERS,
  GET_PROMOTERS_FAILED,
  GET_PROMOTERS_SUCCESS,
  GET_PROMOTER_EVENTS,
  GET_PROMOTER_EVENTS_SUCCESS,
  GET_PROMOTER_EVENTS_FAILED,
  FOLLOW_PROMOTER,
  FOLLOW_PROMOTER_SUCCESS,
  FOLLOW_PROMOTER_FAILED,
  GET_ALL_PROMOTERS_SUCCESS,
  GET_PURCHASE_STATUS_SUCCESS,
} from '../actions/userActions';

import {
  PURCHASE_TICKET_SUCCESS,
} from '../actions/eventActions';

const initialState = {
  data: { name: 'user' },
  following: null,
  promoters: null,
  events: null,
};

const userReducer = (state = initialState, action) => {
  switch(action.type) {
    case GET_PURCHASE_STATUS_SUCCESS: {
      return Object.assign({}, state, {
        events: {
          ...state.events,
          [action.eventAddress]: {
            ...state.events[action.eventAddress],
            purchased: action.isPurchased,
          },
        }
      });
    }
    
    case PURCHASE_TICKET_SUCCESS: {
      return Object.assign({}, state, {
        events: {
          ...state.events,
          [action.eventAddress]: {
            ...state.events[action.eventAddress],
            purchased: true,
          },
        },
      });
    }

    case GET_ALL_PROMOTERS_SUCCESS: {
      return Object.assign({}, state, {
        promoters: action.promoters.reduce((prev, address) => {
          prev[address] = { promoterAddress: address };
          return prev;
        }, {}),
      });
    }

    case REGISTER_USER: {
      return Object.assign({}, state, { 
        isRegistering: true,
        error: null,
        address: action.address,
      });
    }

    case REGISTER_USER_SUCCESS: {
      return Object.assign({}, state, { 
        isRegistering: false,
        address: action.address,
        userAddress: action.contractAddress,
      });
    }

    case REGISTER_USER_FAILED: {
      return Object.assign({}, state, { 
        isRegistering: false,
        error: action.error,
      });
    }

    case GET_PROMOTERS: {
      return Object.assign({}, state, {
        getPromoterStatus: {
          isFetching: true,
          error: null,
        },
      });
    }

    case GET_PROMOTERS_SUCCESS: {
      return Object.assign({}, state, {
        getPromoterStatus: {
          isFetching: false,
        },
        following: action.promoters.reduce((prev, promoterAddress) => {
          prev[promoterAddress] = { promoterAddress };
          return prev;
        }, {}),
      });
    }
    
    case GET_PROMOTERS_FAILED: {
      return Object.assign({}, state, {
        getPromoterStatus: {
          isFetching: false,
          error: action.error
        },
      });
    }

    case FOLLOW_PROMOTER: {
      return Object.assign({}, state, {
        following: {
          ...state.following,
          [action.promoterAddress]: {
            promoterAddress: action.promoterAddress,
            followStatus: {
              isFetching: true,
              error: null,
            }
          }
        }
      });
    }

    case FOLLOW_PROMOTER_SUCCESS: {
      return Object.assign({}, state, {
        following: {
          ...state.following,
          [action.promoterAddress]: {
            promoterAddress: action.promoterAddress,
            followStatus: {
              isFetching: false,
              error: null,
            }
          }
        }
      });
    }

    case FOLLOW_PROMOTER_FAILED: {
      return Object.assign({}, state, {
        following: {
          ...state.following,
          [action.promoterAddress]: {
            promoterAddress: action.promoterAddress,
            followStatus: {
              isFetching: false,
              error: action.error,
            }
          }
        }
      });
    }

    case GET_PROMOTER_EVENTS: {
      return Object.assign({}, state, {
        following: {
          ...state.following,
          [action.promoterAddress]: {
            ...state.following[action.promoterAddress],
            getEventsStatus: {
              isFetching: true,
              error: null,
            },
          },
        },
      });
    }

    case GET_PROMOTER_EVENTS_SUCCESS: {
      return Object.assign({}, state, {
        following: {
          ...state.following,
          [action.promoterAddress]: {
            ...state.following[action.promoterAddress],
            getEventsStatus: {
              isFetching: false,
            },
          },
        },
        events: {
          ...state.events,
          [action.event.eventAddress]: {
            ...action.event,
            promoterAddress: action.promoterAddress,
          },
        },
      });
    }

    case GET_PROMOTER_EVENTS_FAILED: {
      return Object.assign({}, state, {
        following: {
          ...state.following,
          [action.promoterAddress]: {
            ...state.following[action.promoterAddress],
            getEventsStatus: {
              isFetching: false,
              error: action.error,
            },
          },
        },
      });
    }

    default: 
      return state;
  }
};

export default userReducer;