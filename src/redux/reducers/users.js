import {
  SELECT_MANAGER,
  CANCEL_MANAGER_REGISTRATION,
  CREATE_EVENT_SUCCESS,
  CREATE_EVENT,
  CREATE_EVENT_FAILED,
  GET_EVENT_BALANCE_SUCCESS,
} from '../actions/managerActions';

import {
  SELECT_PROMOTER,
  CANCEL_PROMOTER_REGISTRATION,
  REGISTER_TO_PROMOTE_EVENT,
  REGISTER_TO_PROMOTE_EVENT_SUCCESS,
  REGISTER_TO_PROMOTE_EVENT_FAILED,
  GET_PROMOTER_EVENTS,
  GET_PROMOTER_EVENTS_SUCCESS,
  GET_PROMOTER_EVENTS_FAILED,
} from '../actions/promoterActions';

import {
  REGISTER_MANAGER,
  REGISTER_MANAGER_SUCCESS,
  REGISTER_MANAGER_FAILED,
  REGISTER_PROMOTER,
  REGISTER_PROMOTER_SUCCESS,
  REGISTER_PROMOTER_FAILED,
} from '../actions/orchestratorActions';

import {
  REGISTER_USER,
} from '../actions/userActions';

const initialState = {
  owner: null,
  managers: null,
  selectedManager: null,
  promoters: null,
  selectedPromoter: null,
  users: null,
  isInitialising: false,
  isInitialised: false,
  error: null,
};

const usersReducer = (state = initialState, action) => {
  switch(action.type) {
    case GET_EVENT_BALANCE_SUCCESS: {
      return Object.assign({}, state, {
        managers: {
          ...state.managers,
          [action.address]: {
            ...state.managers[action.address],
            balances: {
              ...state.managers[action.address].balances,
              [action.eventAddress]: action.balance,
            },
          },
        }
      });
    }
    
    case REGISTER_MANAGER: {
      return Object.assign({}, state, { 
        managers: { 
          ...state.managers,
          [action.address]: {
            address: action.address, 
            isRegistering: true, 
            error: null 
          }
        }
      });
    }

    case REGISTER_MANAGER_SUCCESS: {
      return Object.assign({}, state, {
        managers: {
          ...state.managers,
          [action.address]: {
            address: action.address,
            managerAddress: action.contractAddress,
            isRegistering: false,
          },
        },
      });
    }

    case REGISTER_MANAGER_FAILED: {
      return Object.assign({}, state, {
        managers: {
          ...state.managers,
          [action.address]: {
            address: null,
            managerAddress: null,
            isRegistering: false,
            error: action.error,
          },
        },
      });
    }
    
    case CANCEL_MANAGER_REGISTRATION: {
      const { [action.address]: omit, ...rest } = state.managers;
      return Object.assign({}, state, { 
        managers: rest,
      });
    }

    case SELECT_MANAGER: {
      return Object.assign({}, state, { 
        selectedManagerAddress: action.address,
      });
    }

    case CREATE_EVENT: {
      const existingManager = state.managers[action.address];
      const updatedManager = Object.assign({}, existingManager, {
        createEventStatus: {
          creating: true,
          error: null,
        },
      });

      return Object.assign({}, state, { managers: {
        ...state.managers,
        [action.address]: updatedManager,
      }});
    }

    case CREATE_EVENT_SUCCESS: {
      const existingManager = state.managers[action.address];
      const updatedManager = Object.assign({}, existingManager, {
        createEventStatus: {
          creating: false,
          error: null,
        },
      });

      return Object.assign({}, state, { managers: {
        ...state.managers,
        [action.address]: updatedManager,
      }});
    }

    case CREATE_EVENT_FAILED: {
      const existingManager = state.managers[action.address];
      const updatedManager = Object.assign({}, existingManager, {
        createEventStatus: {
          creating: false,
          error: action.error,
        },
      });

      return Object.assign({}, state, { managers: {
        ...state.managers,
        [action.address]: updatedManager,
      }});
    }

    case REGISTER_PROMOTER: {
      return Object.assign({}, state, { 
        promoters: { 
          ...state.promoters,
          [action.address]: {
            address: action.address, 
            isRegistering: true, 
            error: null 
          },
        }
      });
    }

    case REGISTER_PROMOTER_SUCCESS: {
      return Object.assign({}, state, {
        promoters: {
          ...state.promoters,
          [action.address]: {
            address: action.address,
            promoterAddress: action.contractAddress,
            isRegistering: false,
          },
        },
      });
    }

    case REGISTER_PROMOTER_FAILED: {
      return Object.assign({}, state, {
        promoters: {
          ...state.promoters,
          [action.address]: {
            address: null,
            promoterAddress: null,
            isRegistering: false,
            error: action.error,
          },
        },
      });
    }

    case CANCEL_PROMOTER_REGISTRATION: {
      const { [action.address]: omit, ...rest } = state.promoters;
      return Object.assign({}, state, { 
        promoters: rest,
      });
    }

    case SELECT_PROMOTER: {
      return Object.assign({}, state, { selectedPromoterAddress: action.address });
    }

    case REGISTER_TO_PROMOTE_EVENT: {
      const updatedPromoter = Object.assign({}, state.promoters[action.address], {
        ...state.promoters[action.address],
        promoteEventStatus: {
          isRegistering: true,
          error: null,
        },
      });
      return Object.assign({}, state, { 
        promoters: {
          ...state.promoters,
          [action.address]: updatedPromoter,
        },
      });
    }

    case REGISTER_TO_PROMOTE_EVENT_SUCCESS: {
      const updatedPromoter = Object.assign({}, state.promoters[action.address], {
        ...state.promoters[action.address],
        promoteEventStatus: {
          isRegistering: false,
          error: null,
        },
        events: action.events,
      });
      return Object.assign({}, state, { 
        promoters: {
          ...state.promoters,
          [action.address]: updatedPromoter,
        },
      });
    }

    case REGISTER_TO_PROMOTE_EVENT_FAILED: {
      const updatedPromoter = Object.assign({}, state.promoters[action.address], {
        ...state.promoters[action.address],
        promoteEventStatus: {
          isRegistering: false,
          error: action.error,
        },
      });
      return Object.assign({}, state, { 
        promoters: {
          ...state.promoters,
          [action.address]: updatedPromoter,
        },
      });
    }

    case GET_PROMOTER_EVENTS: {
      return Object.assign({}, state, {
        promoters: {
          ...state.promoters,
          [action.address]: Object.assign({}, state.promoters[action.address], {
            getEventsStatus: {
              isFetching: true,
              error: null,
            },
          }),
        },
      });
    }

    case GET_PROMOTER_EVENTS_SUCCESS: {
      return Object.assign({}, state, {
        promoters: {
          ...state.promoters,
          [action.address]: Object.assign({}, state.promoters[action.address], {
            getEventsStatus: {
              isFetching: false,
            },
            events: action.events,
          }),
        },
      });
    }

    case GET_PROMOTER_EVENTS_FAILED: {
      return Object.assign({}, state, {
        promoters: {
          ...state.promoters,
          [action.address]: Object.assign({}, state.promoters[action.address], {
            getEventsStatus: {
              isFetching: false,
              error: action.error,
            },
          }),
        },
      });
    }

    case REGISTER_USER: {
      return Object.assign({}, state, { 
        users: { 
          ...state.users,
          [action.address]: {
            address: action.address,
          },
        },
      });
    }

    default:
      return state;
  }
}

export default usersReducer;
