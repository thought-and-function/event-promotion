import getWeb3 from './getWeb3';
import contract from 'truffle-contract';

import EventContractCompiled from '../../build/contracts/Event.json';

async function GetEventContract (address) {
  const { web3 } = await getWeb3;

  let event = contract(EventContractCompiled);
  event.setProvider(web3.currentProvider);
  return event.at(address);
}

export default GetEventContract;