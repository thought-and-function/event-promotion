import getWeb3 from './getWeb3';
import contract from 'truffle-contract';

import EventManagerContractCompiled from '../../build/contracts/EventManager.json';

async function GetEventManagerContract (address) {
  const { web3 } = await getWeb3;

  let eventManager = contract(EventManagerContractCompiled);
  eventManager.setProvider(web3.currentProvider);
  return eventManager.at(address);
}

export default GetEventManagerContract;