import getWeb3 from './getWeb3';
import contract from 'truffle-contract';

import OrchestratorCompiled from '../../build/contracts/Orchestrator.json';

let accounts;

export const getAccounts = () => {
  return accounts;
}

async function setDefaultAccount(web3) {
  accounts = await new Promise((resolve, reject) => {
    web3.eth.getAccounts((error, accounts) => {
      if (error) return reject(error);
      else {
        resolve(accounts);
      }
    });  
  })

  const selectedAccount = accounts && accounts[0];
  if (!selectedAccount) throw Error(`We couldn't select an account to use for transactions. Make sure you are logged in to your wallet provider.`);
  
  web3.eth.defaultAccount = selectedAccount;
  return web3;
}

async function OrchestratorSetup () {
  const { web3 } = await getWeb3;
  await setDefaultAccount(web3);

  let orchestrator = contract(OrchestratorCompiled);
  orchestrator.setProvider(web3.currentProvider);
  return orchestrator.deployed();
}

const OrchestratorContract = OrchestratorSetup()

export default OrchestratorContract;