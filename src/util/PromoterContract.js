import getWeb3 from './getWeb3';
import contract from 'truffle-contract';

import PromoterContractCompiled from '../../build/contracts/Promoter.json';

async function GetPromoterContract (address) {
  const { web3 } = await getWeb3;

  let promoter = contract(PromoterContractCompiled);
  promoter.setProvider(web3.currentProvider);
  return promoter.at(address);
}

export default GetPromoterContract;