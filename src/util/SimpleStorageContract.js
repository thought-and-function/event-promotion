import getWeb3 from './getWeb3';
import contract from 'truffle-contract';

import SimpleStorageCompiled from '../../build/contracts/SimpleStorage.json';

let accounts;

export const getAccounts = () => {
  return accounts;
}

async function setDefaultAccount(web3) {
  accounts = await new Promise((resolve, reject) => {
    web3.eth.getAccounts((error, accounts) => {
      if (error) return reject(error);
      else {
        resolve(accounts);
      }
    });  
  })

  const selectedAccount = accounts && accounts[0];
  if (!selectedAccount) throw Error(`We couldn't select an account to use for transactions. Make sure you are logged in to your wallet provider.`);
  
  web3.eth.defaultAccount = selectedAccount;
  return web3;
}

async function SimpleStorageSetup () {
  const web3Instance = await getWeb3;
  const web3WithAccount = await setDefaultAccount(web3Instance.web3);

  let simpleStorage = contract(SimpleStorageCompiled);
  simpleStorage.setProvider(web3WithAccount.currentProvider);
  return simpleStorage.deployed();
}

const SimpleStorageContract = SimpleStorageSetup()

export default SimpleStorageContract;