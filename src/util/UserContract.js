import getWeb3 from './getWeb3';
import contract from 'truffle-contract';

import UserContractCompiled from '../../build/contracts/User.json';

async function GetUserContract (address) {
  const { web3 } = await getWeb3;

  let user = contract(UserContractCompiled);
  user.setProvider(web3.currentProvider);
  return user.at(address);
}

export default GetUserContract;