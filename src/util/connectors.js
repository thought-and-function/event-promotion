import { Connect, SimpleSigner } from 'uport-connect'

export const uport = new Connect('event-promotion', {
    clientId: '2onaVcNfK7Pnf8Tvhuk6p688dKi5twXeWuv',
    network: 'rinkeby',
    signer: SimpleSigner('c34963268716ab7643dcb24d7eeee85609bc1ee4a9548b0b45c3b2959c0811f6')
});

export const web3 = uport.getWeb3();