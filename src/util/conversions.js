export const ethToWei = (eth) => {
  return eth * 1000000000000000000;
}

export const weiToEth = (wei) => {
  return wei / 1000000000000000000;
}