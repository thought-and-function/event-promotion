const Event = artifacts.require("./Event.sol");

contract(`Event`, (accounts) => {
  let event;
  let promoterContractAddress;
  let totalNumberOfTickets = 100;
  let comissionFee = 10000;
  let ticketPrice = 1000000000;
  let endDate = (new Date()).getTime();

  const [
    admin,
    managerUser,
    promoterUser,
    user,
    invalidAddress,
  ] = accounts;

  beforeEach(`setup contract`, async () => {
    event = await Event.new(totalNumberOfTickets, comissionFee, ticketPrice, endDate, { from: managerUser });
  });

  describe(`registering a promoter`, () => {
    it(`should not fail`, async () => {
      await event.registerAsPromoter({from: promoterUser});
      assert.isOk(true);
    });
  });

  describe(`buying a ticket`, () => {
    it(`should not fail with valid promoter address`, async () => {
      await event.registerAsPromoter({ from: promoterUser });
      await event.buyTicket(promoterUser, { from: user, value: ticketPrice });
      assert.isOk(true);
    });

    it(`should fail if invalid promoter address`, async () => {
      try {
        await event.buyTicket(invalidAddress, { from: user, value: ticketPrice });
      } catch(error) {
        assert.isOk(true);
      }
      assert.isOk(true);
    });
    
    it(`should fail if value less than ticket price`, async () => {
      try {
        await event.buyTicket(promoterUser, { from: user, value: ticketPrice - 100 });
      } catch(error) {
        assert.isOk(true);
      }
      assert.isOk(true);
    });
  });

  describe(`get event balance`, () => {
    it(`should not fail if from event owner`, async () => {
      const balance = await event.getEventBalance({ from: managerUser });
      assert.isOk(true);
    });

    it(`should return correct balance`, async () => {
      await event.registerAsPromoter({from: promoterUser });
      await event.buyTicket(promoterUser, { from: user, value: ticketPrice });
      const balance = await event.getEventBalance({ from: managerUser });
      assert.equal(balance.toNumber(), ticketPrice - comissionFee, "Expected a different balance");
    });
  });

  describe(`withraw balance`, () => {
    it(`should fail if not owner`, async () => {
      try {
        await event.withdrawBalance({ from: promoterUser });
      } catch (erro) {
        assert.isOk(true);
      }
      assert.isOk(true);
    });
  });
});
