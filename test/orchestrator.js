const Orchestrator = artifacts.require("./Orchestrator.sol");

contract(`Orchestrator`, (accounts) => {
  let orchestrator;
  const [
    admin,
    managerUser,
    promoterUser,
    user,
    validAddress,
  ] = accounts;

  beforeEach(`setup contract`, async () => {
    orchestrator = await Orchestrator.deployed();
  });

  let managerContractAddress;
  describe(`registering a manager`, () => {
    it(`should not fail`, async () => {
      const tx = await orchestrator.registerEventManager({from: managerUser});
      managerContractAddress = tx.logs[0].args.managerAddress;
      assert.isOk(true);
    });
  });

  describe(`get manager address`, () => {
    it(`should return manager contract address`, async () => {
      const actual = await orchestrator.getManagerAddress({ from: managerUser });
      assert.equal(managerContractAddress, actual, "Manager address does not match expected");
    });
  }); 

  let promoterContractAddress;
  describe(`registering a promoter`, () => {
    it(`should not fail`, async () => {
      const tx = await orchestrator.registerPromoter({from: promoterUser});
      promoterContractAddress = tx.logs[0].args.promoterAddress;
      assert.isOk(true);
    });
  });

  describe(`get promoter address`, () => {
    it(`should return promoter contract address`, async () => {
      const actual = await orchestrator.getPromoterAddress({ from: promoterUser });
      assert.equal(promoterContractAddress, actual, "Promoter address does not match expected");
    });
  });

  let userContractAddress;
  describe(`registering a user`, () => {
    it(`should not fail`, async () => {
      const tx = await orchestrator.registerUser({from: user});
      userContractAddress = tx.logs[0].args.userAddress;
      assert.isOk(true);
    });
  });

  describe(`get user address`, () => {
    it(`should return user contract address`, async () => {
      const actual = await orchestrator.getUserAddress({ from: user });
      assert.equal(userContractAddress, actual, "User address does not match expected");
    });
  });

  let events;
  describe(`registering an existing event`, () => {
    it(`should not fail with valid address`, async () => {
      await orchestrator.registerEvent(validAddress);
      assert.isOk(true);
      events = [validAddress];
    });

    it(`should fail if address is invalid`, async () => {
      try {
        await orchestrator.registerEvent();
      } catch(error) {
        assert.isOk(true);
      }
      assert.isOk(true);
    });
  });

  describe(`getting events`, () => {
    it(`should return events registered`, async () => {
      const actual = await orchestrator.getEvents();
      assert(actual[0], events[0], "Expected event different from actual");
      assert(actual.length, events.length, "Received the wrong number of events");
    });
  });

  describe(`getting managers`, () => {
    it(`should return managers`, async () => {
      const actual = await orchestrator.getManagers();
      assert(actual[0], managerContractAddress, "Expected manager different");
      assert(actual.length, 1, "Received wrong number of managers");
    });
  });

  describe(`getting promoters`, () => {
    it(`should return promoters`, async () => {
      const actual = await orchestrator.getPromoters();
      assert(actual[0], promoterContractAddress, "Expected promoter different");
      assert(actual.length, 1, "Received wrong number of promoters");
    });
  });
});
